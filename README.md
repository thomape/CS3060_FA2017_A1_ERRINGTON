There are two key files:
	- Errington_A1.rb - this holds all of the methods/class/etc
	- Errington_A1_run.rb - this holds all of the method calls

Other files included are:
	- Hamming.txt - contains the binary information for method 8
	- test.csv - CSV file that holds dummy data for function nine

I ran the program through Bash on windows:
	- Update & Security > For Developers > Developer Mode
	- Control Panel > Programs > Turn Windows Features on or Off ***
	- Tick Windows Subsystems for Linux (Beta)
	- sudo apt-get update
	- sudo apt-get install ruby
	- ruby Errington_A1_run.rb

	*** A system restart will likely be required here


The console will step through each of the methods sometimes asking for user input.  I did NOT
include input error checking.  I used the values that were suggested from the assignment PDF. 
For problems with huge computational requirements such as the matrix and Fibonacci methods, I tested with the smaller values.

Input Values that I used to test:
Method 1 - none
Method 2 - book
		 - ook
		*** This will return a no go if the substring is not found
Method 3 - Hard coded array of size 100 - does appropriate things
Method 4 - Hard coded array of size 100 - does appropriate things
Method 5 - Input 10 to get 55 which is the correct value
		*** Large values will take very long
Method 6 - input 500 - gives the values up to that number
Method 7 - Input 5, 5 - Creates two 5x5 matrices and then multiples them
		*** Large values can be used but will have huge outputs
Method 8 - Hard coded text file location - output goes to screen not file
Method 9 - Input path of csv file - test.csv was used and works
Method 10 - Hardcoded values for the method calls - tried to call every one showing inheritance 

Methods and important breaks are indicated by ******************

Sample Output of program:

zeroCool@THOMAPE58:/mnt/c/Users/Tom/Desktop/BGSU/Fall 2017/CS_3060/A1/CS3060_FA2017_A1_ERRINGTON$ ruby Errington_A1_run.rb
***********Problem 1***********
Hello, World.
***********Problem 2***********
Enter a string:
book
Enter a substring:
ook
1
***********Problem 3***********
91
69
68
75
54
83
47
27
75
95
35
74
25
28
93
15
50
46
71
35
68
29
76
34
84
6
96
71
32
18
80
44
45
41
53
58
21
51
49
9
54
15
71
98
61
67
89
88
30
61
24
58
55
18
76
91
10
31
21
87
10
93
76
15
81
34
68
86
45
87
97
2
8
38
81
19
51
60
42
100
45
19
21
95
38
74
98
58
63
16
93
42
71
62
92
90
70
38
65
6
******************
2
6
6
8
9
10
10
15
15
15
16
18
18
19
19
21
21
21
24
25
27
28
29
30
31
32
34
34
35
35
38
38
38
41
42
42
44
45
45
45
46
47
49
50
51
51
53
54
54
55
58
58
58
60
61
61
62
63
65
67
68
68
68
69
70
71
71
71
71
74
74
75
75
76
76
76
80
81
81
83
84
86
87
87
88
89
90
91
91
92
93
93
93
95
95
96
97
98
98
100
******************
100
98
98
97
96
95
95
93
93
93
92
91
91
90
89
88
87
87
86
84
83
81
81
80
76
76
76
75
75
74
74
71
71
71
71
70
69
68
68
68
67
65
63
62
61
61
60
58
58
58
55
54
54
53
51
51
50
49
47
46
45
45
45
44
42
42
41
38
38
38
35
35
34
34
32
31
30
29
28
27
25
24
21
21
21
19
19
18
18
16
15
15
15
10
10
9
8
6
6
2
******************
98
97
95
93
93
91
90
88
87
84
81
80
76
75
74
71
71
70
68
68
65
62
61
58
58
54
53
51
49
46
45
44
42
38
38
35
34
31
29
27
24
21
19
18
16
15
10
9
6
2
***********Problem 4***********
51
70
48
9
79
58
80
97
12
49
89
84
5
40
15
85
68
58
25
90
81
64
7
8
42
62
36
78
99
15
27
98
24
33
54
59
97
82
39
16
28
71
12
90
2
91
80
75
88
77
61
45
20
51
47
48
70
81
28
11
77
30
42
90
100
49
58
9
99
44
100
15
69
26
75
100
90
69
5
87
56
33
80
3
2
73
62
15
39
59
20
44
50
100
45
73
67
33
77
59
2
2
3
5
5
7
8
9
9
11
12
12
15
15
15
15
16
20
20
24
25
26
27
28
28
30
33
33
33
36
39
39
40
42
42
44
44
45
45
47
48
48
49
49
50
51
51
54
56
58
58
58
59
59
59
61
62
62
64
67
68
69
69
70
70
71
73
73
75
75
77
77
77
78
79
80
80
80
81
81
82
84
85
87
88
89
90
90
90
90
91
97
97
98
99
99
100
100
100
100
***********Problem 5***********
Enter a number between 1 and 500:
10
55
***********Problem 6***********
Enter a number between 1 and 500:
500
1
1
2
3
5
8
13
21
34
55
89
144
233
377
***********Problem 7***********
First matrix size: 50, 100, 500, 1000
5
Second matrix size: 50, 100, 500, 1000
5
Matrix[[9712, 7093, 9918, 11921, 11824], [4209, 7925, 14738, 9663, 15064], [6527, 9145, 16248, 12922, 14722], [14129, 11531, 23498, 19946, 23226], [3739, 5668, 12112, 7061, 11864]]
***********Problem 8***********
1001111:1010011:3
***********Problem 9***********
Enter the location of the CSV file( ie: my/csv/location)
test.csv
["﻿Number", "First", "Last", "Age", "Height", "Eye Color", "Hair Color", "Transportation"]
["1", "Bob", "Johnson", "23", "72", "Blue", "Brown", "Ferrari"]
["2", "Bryan", "White", "33", "73", "Blue", "Brown", "Prius"]
["3", "Steve", "Wholly", "63", "68", "Brown", "Blonde", "Lambo"]
["4", "Frank", "Drake", "35", "72", "Green", "Blonde", "Truck"]
["5", "Justin", "Molina", "23", "60", "Blue", "Black", "Bike"]
["6", "Tim", "Evans", "72", "82", "Brown", "Red", "Walk"]
["7", "Tim", "Muir", "23", "60", "Brown", "Bald", "Wheelchair"]
["8", "Eric", "House", "26", "70", "Brown", "Bald", "Mustang"]
["9", "Mike", "Weber", "25", "71", "Blue", "Brown", "Unicycle"]
["10", "Tex", "German", "24", "71", "Green", "Red", "Hovercraft"]
***********Problem 10**********
Shape class test*****************
Red
true
Yellow
false
Brown
true
Circle class test*******************
1.0
10.0
20.0
Orange
false
30.0
2826.0
188.4
Rectangle class test*****************
1.0
1.0
3.0
3.0
9.0
5.0
false
Tan
12.0
10.0
120.0
44.0
Square class test******************
1.0
3.0
pink
false
12.0
144.0
48.0