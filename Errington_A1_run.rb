# This file includes all of the method calls
# Steps through each problem

require_relative "Errington_A1"
require 'matrix'
require 'csv'

puts "***********Problem 1***********"

# prints what is in the problem one function

puts problemOne

puts "***********Problem 2***********"

# gets user input, uses chomp to delete carriage return 
puts "Enter a string: "
string = gets.chomp

# gets user input, uses chomp to delete carriage return 
puts "Enter a substring: "
substring = gets.chomp

puts problemTwo(string, substring)  # Will display the index of the beginning match

puts "***********Problem 3***********"

# creates new array within function call and fills randomly
problemThree(Array.new(100) { rand(1..100)})

puts "***********Problem 4***********"

# creates new array within function call and fills randomly
problemFour(Array.new(100) { rand(1..100)})

puts "***********Problem 5***********"

# gets user input, uses chomp to delete carriage return 
puts "Enter a number between 1 and 500: "
fibs = gets.chomp

puts problemFive(fibs)	# function call

puts "***********Problem 6***********"

# gets user input, uses chomp to delete carriage return 
puts "Enter a number between 1 and 500: "
newFibs = gets.chomp

problemSix(newFibs)	# function call

puts "***********Problem 7***********"

# gets user input, uses chomp to delete carriage return 
puts "First matrix size: 50, 100, 500, 1000"
matOne = gets.chomp
matOneInt = matOne.to_i	# convert to int

# creates matrix based in input size fills with rand
matrixA = Matrix.build(matOneInt) {rand(1..100)} 

# gets user input, uses chomp to delete carriage return 
puts "Second matrix size: 50, 100, 500, 1000"
matTwo = gets.chomp
matTwoInt = matTwo.to_i # convert to int

# creates matrix based in input size fills with rand
matrixB = Matrix.build(matTwoInt) {rand(1..100)} 

puts problemSeven(matrixA, matrixB) # function call

puts "***********Problem 8***********"

# uses this file in the directory with the stored binary values
myFile = "Hamming.txt"

# calls function with file location
problemEight(myFile)

puts "***********Problem 9***********"

# asks for the file location
puts "Enter the location of the CSV file( ie: my/csv/location)"
myCSV = gets.chomp

# calls the function with the user input location
problemNine(myCSV)

puts "***********Problem 10**********"

puts "Shape class test*****************"
# create new shape
myShape = Shape.new

# test default
myShape.shape
puts myShape.getColor
puts myShape.isFilled

# test constructor
myShape.shapeMine("Yellow", false)
puts myShape.getColor
puts myShape.isFilled

# test setter and getters
myShape.setColor("Brown")
puts myShape.getColor
myShape.setFilled(true)
puts myShape.isFilled

puts "Circle class test*******************"
# create new circle
myCircle = Circle.new

# test default constructor
myCircle.circle
puts myCircle.getRadius

# constructor
myCircle.circleRad(10.0)
puts myCircle.getRadius

# another 
myCircle.circleMine(20.0, "Orange", false)
puts myCircle.getRadius
puts myCircle.getColor
puts myCircle.isFilled

#getter and setter
myCircle.setRadius(30.0)
puts myCircle.getRadius
puts myCircle.getArea
puts myCircle.getPerimeter

puts "Rectangle class test*****************"

myRect = Rectangle.new

# test default constructor
myRect.rectangle
puts myRect.getLength
puts myRect.getWidth

# constructor
myRect.rectangleMine(3.0, 3.0)
puts myRect.getLength
puts myRect.getWidth

# another
myRect.rectangleLong(5.0, 9.0, "Tan", false)
puts myRect.getLength
puts myRect.getWidth
puts myRect.isFilled
puts myRect.getColor

# getter and setter
myRect.setLength(12.0)
myRect.setWidth(10.0)
puts myRect.getLength
puts myRect.getWidth
puts myRect.getArea
puts myRect.getPerimeter

puts "Square class test******************"

mySquare = Square.new

# default
mySquare.square
puts mySquare.getSide

# constructor
mySquare.squareMine(3.0, "pink", false)
puts mySquare.getSide
puts mySquare.getColor
puts mySquare.isFilled

# getter and setters
mySquare.setSide(12.0)
puts mySquare.getSide

# other methods
puts mySquare.getArea
puts mySquare.getPerimeter






