# Problem one
def problemOne
	# holds the value to print
	'Hello, World.'
end

#Problem two
def problemTwo(string, substring)
	# checks to see if the substring is included in the string
	if string.include? substring

		# if the substring is containted then it will return the index of the start value
		string.index(substring)
	else
		print "Nope" # doesnt find a match
	end
end

# Problem three
def problemThree(array)
	# Displays randomly generated array
	array.each { |x| puts x }
	
	puts "******************"
	# Creates a new array and sorts array[] in ascending order
	sorted = array.sort 
	sorted.each { |y| puts y }

	puts "******************"
	# Creates a new array an sorts sorted[] in reverse
	reversed = sorted.reverse
	reversed.each { |z| puts z }

	puts "******************"
	# 
	oddArray = reversed.select.with_index { |_, i| i.odd? }
	oddArray.each { |a| puts a }

end

# Problem four
def problemFour(array)
	# Displays the initial randomly generated array
	array.each { |x| puts x }

	# assign a tracker variable for the size of the array
	 tracker = array.size

	 # nested for loops to inspect elements next to each other and replace if requrired
	 for i in 0..tracker
	 	for i in 0..tracker
	 		for z in (i + 1)..tracker
	 			if array[z - 1] < array[i - 1]
	 				holder = array[z - 1]
	 				array[z - 1] = array[i - 1]
	 				array[i - 1] = holder
				end	
	 		end
	 	end
	 end

	 # prints sorted array
	  array.each { |x| puts x }

  end

  # Problem five
  def problemFive(fibs)

  	# convert string to int
  	intFibs = fibs.to_i 

  	# return if less than one fib wont work
  	return intFibs if  intFibs <= 1

  	# recursive call to the function 
  	(problemFive(intFibs -1) + problemFive(intFibs - 2))
  end

# Problem six
def problemSix(newFibs)

	# convert string to int
	newIntFibs  = newFibs.to_i

	# return less than one fib wont work
	if newIntFibs <= 1
		return newIntFibs
	end	


	x = 0 
	y = 1

	# loop to calculate fibs up to input number
	while y < newIntFibs do
		puts y

		x,y = y, x + y
	end
end

# problem seven
def problemSeven(matrixA, matrixB)

	 # create new matrix based on size of two matrix parameters
	matrixC = Matrix.build(matrixA.rank * matrixB.rank)

	# do the matrix multiplation
	matrixC = (matrixA * matrixB)

	# return the newly filled matrix to print
	return matrixC
end

# problme eight
def problemEight(myFile)


	# opens the passed in file in read mode then iterates through the lines
	# and adding new elements to an array seperated by a comma
	File.open(myFile, "r").each_line do |line|
		vars = line.split(",")

		# create a new array from element zero that stores each value in a 
		# new element
		first = vars[0]
		numOne = first.split('')
		#numOne.each { |x| puts x }

		# create a new array from element zero that stores each value in a 
		# new element
		second = vars[1]
		numTwo = second.split('')
		#numTwo.each { |y| puts y }

		# a simple loop that compares each element of the two arrays and 
		# adds a value to count if there is a difference between the two
		i = 0
		count = 0
		while ( i <= numOne.size)
			
				if (numOne[i] != numTwo[i])
					count = count + 1
				end
				i = i + 1
			
		end
		
		# Prints to the console not the file, couldnt get it to print to file without overwriting data

		#IO.write(myFile, message = vars[0] + ":" + vars[1] + ":" + count.to_s)
		print vars[0]
		print ":"
		print vars[1]
		print ":"
		puts count
end

end

#problem 9
def problemNine(myCSV)

	# uses the CSV class to iterate through and add each line to an array
	# then prints each line on screen
	CSV.foreach(myCSV) do |row|
		puts row.inspect
	end
	
end

# problem 10
# Shape Class
class Shape
	# private
	attr :color
	attr :filled

	# default constructor
	def shape
		@color = "Red"
		@filled = true
	end

	# Constructor
	def shapeMine (aColor, isFilled)
		@color = aColor
		@filled = isFilled
	end

	# getting method for color
	def getColor
		return @color
	end

	# setter method for color
	def setColor(newColor)
		@color = newColor
	end

	# getter for fill
	def isFilled
		return @filled
	end

	# setter for fill
	def setFilled(fill)
		@filled = fill
	end
end

# Circle class
class Circle < Shape

	#Private 
	attr :radius

	# Default constructor
	def circle
		@radius = 1.0
	end

	# constructor
	def circleRad(myRadius)
		@radius = myRadius
	end

	# constructor
	def circleMine(myRad, myColor, myFill)
		@radius = myRad
		@color = myColor
		@filled = myFill
	end

	# getter for radius
	def getRadius
		return @radius
	end

	# setter for radius
	def setRadius(rad)
		@radius = rad
	end

	# getter for area
	def getArea
		return (@radius * @radius) * 3.14
	end

	def getPerimeter
		return (2 * 3.14 * @radius)
	end
end

# Rectangle Class 
class Rectangle < Shape

	# Private 
	attr :width
	attr :length

	# default Constructor
	def rectangle 
		@width = 1.0
		@length = 1.0
	end

	# constructor
	def rectangleMine(myWidth, myLength)
		@width = myWidth
		@length = myLength
	end

	# another long
	def rectangleLong(myWidth, myLength, myColor, myFill)
		@width = myWidth
		@length = myLength
		@color = myColor
		@filled = myFill
	end

	# getter for width
	def getWidth
		return @width
	end

	# setter for width
	def setWidth(myWidth)
		@width = myWidth
	end

	# getter for length
	def getLength
		return @length
	end

	# setter for length
	def setLength(myLength)
		@length = myLength
	end

	# getter for area
	def getArea
		return @length * @width
	end

	# getter for perimeter
	def getPerimeter
		return (2 * @length) + (2 * @width)
	end
end

# Square class 
class Square < Rectangle

	# default constructor
	def square
		@width = 1.0
		@length = 1.0
	end

	# constructor
	def squareMine(mySide, myColor, myFill)
		@width = mySide
		@color = myColor
		@filled = myFill
	end

	# getter for side
	def getSide
		return @width
	end

	# setter for side
	def setSide(mySide)
		@width = mySide
		@length = mySide
	end
end